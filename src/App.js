import "./App.css";
import MapContainer from "./comps/mapContainer/MapContainer";
import TestReq from "./comps/TestReq";

function App() {
  return (
    <div className="App">
      wow this website is cool
      <TestReq />
      <MapContainer />
    </div>
  );
}

export default App;

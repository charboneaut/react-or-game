import { useState } from "react";

export default function TestReq() {
  const url =
    process.env.NODE_ENV === "production"
      ? "https://react-or-demo-game.vercel.app/api"
      : "http://localhost:3000/api";

  const [name, setName] = useState("loser");
  const [text, setText] = useState(getText);

  function getText() {
    fetch(`${url}/hello?name=${name}`)
      .then((res) => res.json())
      .then((data) => setText(data));
  }

  function getNewText() {
    fetch(`${url}/goodbye?name=${name}`)
      .then((res) => res.json())
      .then((data) => setText(data));
  }

  return (
    <div>
      <input onChange={(event) => setName(event.target.value)}></input>
      <br />
      {text}
      <br />
      <button onClick={getNewText}>cya</button>
    </div>
  );
}

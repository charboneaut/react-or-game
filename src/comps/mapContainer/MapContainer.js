import { maps } from "../../maps";
import "./MapContainer.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import { v4 } from "uuid"


export default function MapContainer() {
  return (
    <Container>
      {maps.map1.map((row) => (
        <Row key={v4()}>
          {row.map((tile) => {
            if (!tile) {
              return <span className="empty" key={v4()}></span>;
            } else {
              return <span className="open" key={v4()}></span>;
            }
          })}
        </Row>
      ))}
    </Container>
  );
}

module.exports = (req, res) => {
  const name = req.query.name;
  res.status(200).json(`Hello ${name}!`);
};
